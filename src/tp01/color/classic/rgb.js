const Rgba = require('./rgba')

class Rgb extends Rgba {
  constructor (r, g, b) {
    super(r, g, b, 1)
  }
}

module.exports = Rgb
