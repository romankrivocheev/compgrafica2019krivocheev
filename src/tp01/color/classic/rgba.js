class Rgba {
  constructor (r = 1, g = 1, b = 1, a = 1) {
    this.r = r
    this.g = g
    this.b = b
    this.a = a
  }

  vec4 () {
    let vec4 = []
    vec4[0] = this.r
    vec4[1] = this.g
    vec4[2] = this.b
    vec4[3] = this.a
    return vec4
  }

  vec3 () {
    let vec3 = []
    vec3[0] = this.r
    vec3[1] = this.g
    vec3[2] = this.b
    return vec3
  }
}

module.exports = Rgba
