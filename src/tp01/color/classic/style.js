const Hex = require('./hex')

class Style extends Hex {
  constructor (styleString) {
    let hexNumber = styleString.substring(1, styleString.length)
    if (hexNumber.length === 6) {
      super(parseInt(hexNumber, 16))
    } else if (hexNumber.length === 3) {
      let tempNumber = hexNumber
      hexNumber = tempNumber[0]
      hexNumber += tempNumber[0]
      hexNumber += tempNumber[1]
      hexNumber += tempNumber[1]
      hexNumber += tempNumber[2]
      hexNumber += tempNumber[2]
      super(parseInt(hexNumber, 16))
    } else {
      super(0)
    }
  }
}

module.exports = Style
