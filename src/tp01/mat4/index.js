const transpose = (a) => {
  // Para cuadradas solamente
  let aSize = Math.sqrt(a.length)
  let transposed = []
  for (let offset = 0; offset < aSize; offset += 1) {
    for (let i = 0; i < aSize; i += 1) {
      transposed[i + offset * aSize] = a[i * aSize + offset]
    }
  }
  return transposed
}

const multiply = (a, b) => {
  let multiplied = []
  let length = Math.sqrt(a.length)
  for (let i = 0; i < length; i += 1) {
    for (let offset = 0; offset < length; offset += 1) {
      let tempResult = 0
      for (let j = 0; j < length; j += 1) {
        tempResult += a[i * length + j] * b[j * length + offset]
      }
      multiplied.push(tempResult)
    }
  }
  return multiplied
}

module.exports = {
  transpose,
  multiply
}
