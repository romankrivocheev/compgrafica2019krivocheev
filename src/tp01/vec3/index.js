const normalize = (a) => {
  let normalvec = Math.sqrt(Math.pow(a[0], 2) + Math.pow(a[1], 2) + Math.pow(a[2], 2))
  let normalized = []
  normalized[0] = a[0] / normalvec
  normalized[1] = a[1] / normalvec
  normalized[2] = a[2] / normalvec
  console.log(normalized)
  return normalized
}

const cross = (a, b) => {
  let crossResult = []
  crossResult[0] = a[1] * b[2] - a[2] * b[1]
  crossResult[1] = -(a[0] * b[2] - a[2] * b[0])
  crossResult[2] = a[0] * b[1] - a[1] * b[0]
  return crossResult
}

const normals = (a, b, c) => {
  let vectorOne = []
  let vectorTwo = []
  vectorOne[0] = c[0] - a[0]
  vectorOne[1] = c[1] - a[1]
  vectorOne[2] = c[2] - a[2]
  vectorTwo[0] = b[0] - a[0]
  vectorTwo[1] = b[1] - a[1]
  vectorTwo[2] = b[2] - a[2]
  let tempResult = cross(vectorTwo, vectorOne)
  tempResult = normalize(tempResult)
  return tempResult
}

module.exports = {
  normalize,
  cross,
  normals
}
