const Rgb = require('./rgb')

class Hex extends Rgb {
  constructor (hexNumber) {
    let hex = Math.floor(hexNumber)

    let r = (hex >> 16 & 255) / 255
    let g = (hex >> 8 & 255) / 255
    let b = (hex & 255) / 255
    super(r, g, b)
  }
}

module.exports = Hex
