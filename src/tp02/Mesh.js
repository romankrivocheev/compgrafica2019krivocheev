
class Mesh {
  constructor (geometry, material) {
    this.geometry = geometry
    this.material = material
    this.sx = 1.0
    this.sy = 1.0
    this.sz = 1.0

    // VBO vertex buffer object
    // IBO index buffer object
    this.VBOData = new Float32Array(this.geometry.vertices)
    this.IBOData = new Uint16Array(this.geometry.faces)
  }
  addMesh (mesh) {
  }
  getModelMatrix () {
    return this.modelMatrix
  }
}

module.exports = Mesh
