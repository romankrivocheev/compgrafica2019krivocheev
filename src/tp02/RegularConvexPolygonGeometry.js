const Geometry = require('./Geometry')

class RegularConvexPolygonGeometry extends Geometry {
  constructor (edges) {
    var vertices = []
    var faces = []
    for (let i = 0; i < 360; i += 360 / edges) {
      vertices.push(Math.cos(i * Math.PI / 180))
      vertices.push(Math.sin(i * Math.PI / 180))
      vertices.push(0)
      // console.log(vertices)
    }
    for (let i = 0; i < edges - 2; i++) {
      faces.push(0)
      faces.push(i + 1)
      faces.push(i + 2)
    }
    super(vertices, faces)
  }
}

module.exports = RegularConvexPolygonGeometry
