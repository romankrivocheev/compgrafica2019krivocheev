
class Scene {
  constructor (clearColor) {
    this.meshes = []
    this.clearColor = clearColor
  }
  addMesh (mesh) {
    this.meshes.push(mesh)
  }
}
module.exports = Scene
