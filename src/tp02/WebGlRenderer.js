const { resizeCanvas } = require('./utils')
var vsSource = require('./vertex-shader.glsl') // Averiguar que es
var fsSource = require('./fragment-shader.glsl') // Averiguar que es

class WebGlRenderer {
  constructor (canvas) {
    var gl = canvas.getContext('webgl')

    var vertexShader = gl.createShader(gl.VERTEX_SHADER)
    gl.shaderSource(vertexShader, vsSource)
    gl.compileShader(vertexShader)

    var fragmentShader = gl.createShader(gl.FRAGMENT_SHADER)
    gl.shaderSource(fragmentShader, fsSource)
    gl.compileShader(fragmentShader)

    var program = gl.createProgram()
    gl.attachShader(program, vertexShader)
    gl.attachShader(program, fragmentShader)

    gl.linkProgram(program)
    gl.useProgram(program)

    resizeCanvas(gl.canvas)

    this.gl = gl
    this.program = program
  }
  render (scene, camera) {
    // Retomamos las variables gl y program para usarlas localmente
    var gl = this.gl
    var program = this.program

    // Specify the color for clearing <canvas>
    gl.clearColor(0, 0, 0, 1.0)

    // Clear <canvas>
    gl.clear(gl.COLOR_BUFFER_BIT)

    // Cache attribute/uniform location
    var aPosition = gl.getAttribLocation(program, 'aPosition')
    gl.enableVertexAttribArray(aPosition)
    var uColor = gl.getUniformLocation(program, 'uColor')

    var figureVBOData = new Float32Array(scene.meshes[0].VBOData)

    // Create Vertex Buffer Object
    var bufferVBO = gl.createBuffer()
    gl.bindBuffer(gl.ARRAY_BUFFER, bufferVBO)

    // Populate Buffer Object
    gl.bufferData(gl.ARRAY_BUFFER, figureVBOData, gl.STATIC_DRAW)

    // Bind Buffer to a shader attribute
    gl.vertexAttribPointer(
      aPosition, 3, gl.FLOAT, false,
      figureVBOData.BYTES_PER_ELEMENT * 0, figureVBOData.BYTES_PER_ELEMENT * 0
    )

    // Clean up
    gl.bindBuffer(gl.ARRAY_BUFFER, null)

    gl.uniform4fv(uColor, scene.meshes[0].material)

    var figureIBOData = new Uint16Array(scene.meshes[0].IBOData)

    // Create Index Buffer Object
    var bufferIBO = gl.createBuffer()
    gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, bufferIBO)
    gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, figureIBOData, gl.STATIC_DRAW)
    gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, null)

    // Draw Triangle
    gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, bufferIBO)
    gl.drawElements(gl.TRIANGLES, figureIBOData.length, gl.UNSIGNED_SHORT, 0)
    gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, null)

    // Free buffered memory
    gl.deleteBuffer(bufferVBO)
    gl.deleteBuffer(bufferIBO)
  }
}
module.exports = WebGlRenderer
