const Mesh = require('./Mesh.js')
const WebGLRenderer = require('./WebGLRenderer.js')
const RegularConvexPolygonGeometry = require('./RegularConvexPolygonGeometry.js')
const Scene = require('./Scene.js')

var polygon = new RegularConvexPolygonGeometry(60) // -> Cambiar numero por caras
var material = new Float32Array([1.0, 1.0, 0.0, 1.0])
var mesh = new Mesh(polygon, material)
var scene = new Scene(0.5, 0.5, 0.5)
scene.addMesh(mesh)
var webGLInstance = new WebGLRenderer(document.getElementById('c')) // Document by id -> C ?
webGLInstance.render(scene, 1)
